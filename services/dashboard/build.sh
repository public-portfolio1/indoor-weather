gcloud builds submit ./ \
    --tag=gcr.io/${TF_VAR_google_project_id}/indoor-weather-dashboard \
    --project=${TF_VAR_google_project_id} > indoor-weather-dashboard.containerbuild.log