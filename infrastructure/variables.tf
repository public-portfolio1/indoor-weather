variable "google_bigquery_region" {
    type = string
}

variable "google_project_id" {
    type = string
}

variable "google_region" {
    type = string
}