resource "google_project_service" "cloud_build" {
  project = var.google_project_id
  service = "cloudbuild.googleapis.com"
}