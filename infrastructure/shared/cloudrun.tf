resource "google_project_service" "cloud_run" {
  project = var.google_project_id
  service = "run.googleapis.com"
}