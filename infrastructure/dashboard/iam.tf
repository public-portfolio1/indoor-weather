resource "google_service_account" "dashboard_service_account" {
  account_id   = "dashboard-service-account"
  display_name = "Dashboard Service Account"
}