resource "google_cloud_run_service" "dashboard" {
  name     = "indoor-weather-dashboard"
  location = var.google_region

  template {
    spec {
      containers {
        ports {
          container_port = 3000
        }
        image = "gcr.io/${var.google_project_id}/indoor-weather-dashboard"
        env {
          name  = "PROJECT_NAME"
          value = var.google_project_id
        }
        env {
          name  = "BQ_REGION"
          value = var.google_bigquery_region
        }
      }
      service_account_name = google_service_account.dashboard_service_account.email
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
  autogenerate_revision_name = true
}

resource "google_cloud_run_service_iam_binding" "noauth" {
  location = var.google_region
  project  = var.google_project_id
  service  = "indoor-weather-dashboard"

  role       = "roles/run.invoker"
  members    = ["allUsers"]
  depends_on = [google_cloud_run_service.dashboard]
}