terraform {
    required_version = ">= 1.2"
    required_providers {
      google = {
        source = "hashicorp/google"
        version = "4.27.0"
      }
    }
}

module "shared_services" {
    source = "./shared"
    google_project_id = var.google_project_id
    google_region = var.google_region
}

module "dashboard" {
    source = "./dashboard"
    google_bigquery_region = var.google_bigquery_region
    google_project_id = var.google_project_id
    google_region = var.google_region
}